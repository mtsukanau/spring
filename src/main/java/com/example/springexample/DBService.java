package com.example.springexample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Repository
public class DBService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    //Получаем имя работника по заданному значению source
    public List<String> getName(String source){
        List<String> name = jdbcTemplate.queryForList("select name from employee where source = ?", new Object[] { source }, String.class);
        return name;
    }

    //Получаем имя работника по заданному имени отдела
    public List<String> getNameWithDepartmentName(String departmentName) {
        List<String> named = jdbcTemplate.queryForList("select employee.name from employee inner join department on employee.department_id = department.id WHERE department.NAME = ?" , new Object[] {departmentName}, String.class);
        return named;
    }

    //Создаем запись с заполненным полем source
    public boolean createField(String value) {
        jdbcTemplate.update("INSERT INTO employee (source) values (?)", new Object[] {value});
        int rownumber = jdbcTemplate.queryForObject("SELECT count(*) FROM employee", int.class);
        if (rownumber % 10 != 0) {
            return false;
        } else {
            return true;
        }
    }
}