package com.example.springexample;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@ManagedResource
public class NumberRestController {

    static final Logger logger = LoggerFactory.getLogger(NumberRestController.class);

    @ManagedOperation
    public void setEnabled(boolean enabled){
        this.enabled = enabled;
    }

    @Value("${factorial.enabled: true}")
    Boolean enabled;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private DBService dbService;

    @GetMapping(value = "str/{m}", produces = "application/json")
    public String getStr(@PathVariable int m) {
        logger.debug("Convert debug message. M = {}", m);
        return "M = " + Integer.toString(m);
    }

    @GetMapping(value = "/factorial/{p}", produces = "application/json")
    public int getResult(@PathVariable int p) {
        logger.debug("Factorial debug message. P = {}", p);
        int result = 1;
        if (!enabled) {
            result = -1;
        } else {
            for (int i = 1; i <= p; i++) {
                result = result * i;
            }
        }
        return result;
    }

    @GetMapping(value = "/count", produces = "application/json")
    public int getCount(){
        int result = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM EMPLOYEE", Integer.class);
        return result;
    }

    @GetMapping(value = "/name/{source}", produces = "application/json")
    List<String> getEmployeeName(@PathVariable String source) {
        return dbService.getName(source);
    }

    @GetMapping(value = "/named/{departmentName}", produces = "application/json")
    List<String> getEmployeeNameWithDepartmentName(@PathVariable String departmentName) {
        return dbService.getNameWithDepartmentName(departmentName);
    }

    @GetMapping(value = "/create/{value}", produces = "application/json")
    Boolean createFieldInEmployee(@PathVariable String value) {
        return dbService.createField(value);
    }
}