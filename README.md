# SpringExample Application

[![Spring](https://s.dou.ua/img/announces/kenhlaptrinh-java-spring-logo_NjAxb8l_9BAvQBJ_7aGa2ZV.png)](https://spring.io/)

# Description
---
This is my first small program that uses Spring and Maven.

After starting the program, you will receive a greeting message.

You can get a personal greeting. Just add `?name=your_name` to the address line.

## Task 4 Update:
Now my program can calculate the factorial and turn the number into a string.

## Task 5 Update:
Spring Boot Actuator connected.

Default address `/actuator/health` changed to `/status`.


# Usage

### Start from IntelliJ IDEA
---
**Open project**

**Go to** `Application.java`

**Click right mouse button**

**Click** "Debug 'Application main()' "

**Open** `http://localhost:8080/greeting` 

**You will see** "Hello, World!"`
> If you want a personalized greeting you should open `http://localhost:8080/greeting?name=<your_name>`

### Start from Command Line
---
**Go to project directory and run a following commands:**
```
mvn clean
mvn compile
mvn package
cd Project_Directory\target\
java -jar project_name(-1.0-SNAPSHOT).jar
```
**Open** `http://localhost:8080/greeting`

**You will see** "Hello, World!"
> If you want a personalized greeting you should open `http://localhost:8080/greeting?name=your_name` 

### Factorial calculate
---
After compile open `http://localhost:8080/factorial/<number>` in your address bar. 
`<number>` - the number whose factorial you want to calculate.

### Convert number into a string
---
After compile open `http://localhost:8080/str/<number>` in your address bar. 
`<number>` - the number which you want to convert.

# cURL requests

curl http://localhost:8080/str/100
`M = 100`

curl http://localhost:8080/factorial/10
`3628800`

# Tech
---

This application uses [Spring] framework and [Maven].

# License
---
MIT


   [Spring]: <https://spring.io/projects/spring-boot>
   [Maven]: <https://maven.apache.org/>
